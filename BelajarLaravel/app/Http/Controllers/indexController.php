<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function master()
    {
        return view('layouts.master');
    }
    public function table(){
        return view('table');
    }

    public function datatables()
    {
        return view('data-tables');
    }
    public function utama()
    {
        return view('layouts.welcome');
    }
}
